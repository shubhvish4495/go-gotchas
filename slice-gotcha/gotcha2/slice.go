package main

import "fmt"

func main() {
	a := []int{1, 2, 3}

	appendFunc(a)

	fmt.Println(a)
}

func appendFunc(data []int) {
	data = append(data, 100)
}
