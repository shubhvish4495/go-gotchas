# Explanation


As we know Golang is a pass by reference language, so when we pass a variable to a function a copy of the same is sent to the function for use. 
Now in the gotcha example as I showed in this repo we have an array and a slice.

In Go array have the internal data structure similar to what we have in any other language i.e sequential memory block being allocated for data. But for slices it is different. Slice has a internal data structure which has fields len, cap & pointer to elements stored in it.

```go
// We can think of slice to be a data structure similar to this

type slice struct {
    len int // <--- stores the current len
    cap int // <--- total capacity of slice until it is required to increase it's length
    data *[cap]int // <---- pointer to internal data 
}
```

Now once this slice is being passed into a function a copy of this data is being sent. This means the internal pointer to data is also being copied. If we are to update the data inside the slice inside another function, it will also update the original slice as the data copy of slice references the original data only. 

<img src="gotcha1.jpg"/>


So this is the reason when we pass a slice to a function it gets updated but array data is not affected.