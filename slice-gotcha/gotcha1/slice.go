package main

import "fmt"

func main() {

	// array
	a := [5]int{1, 2, 3, 4, 5}
	// slice
	b := []int{1, 2, 3, 4, 5}

	// edit array and slice
	editArray(a)
	editSlice(b)

	// print data
	fmt.Println(a)
	fmt.Println(b)
}

func editArray(data [5]int) {
	data[0] = 0
}

func editSlice(data []int) {
	data[0] = 0
}
